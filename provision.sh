bin

#!/bin/bash --login

sudo apt-get update

# Install Apache2
sudo apt-get -y install apache2
sudo sh -c 'echo "ServerName localhost" >> /etc/apache2/conf.d/name'

# Install MySQL: password is 'root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server
sudo apt-get -y install libmysqlclient-dev

# Install PHP
sudo apt-get -y install php5
sudo apt-get -y install libapache2-mod-php5
sudo apt-get -y install php5-mysql
sudo apt-get -y install php-pear
sudo apt-get -y install php5-xdebug
sudo apt-get -y install php5-gd
sudo apt-get -y install php5-curl

# Install useful tools
sudo apt-get -y install curl
sudo apt-get -y install unzip
sudo apt-get -y install git
sudo apt-get -y install vim

# activate mod_rewrite
if [ ! -L /etc/apache2/mods-enabled/rewrite.load ]; then
    sudo ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
fi

# activate mod_vhost_alias
if [ ! -L /etc/apache2/mods-enabled/vhost_alias.load ]; then
    sudo ln -s /etc/apache2/mods-available/vhost_alias.load /etc/apache2/mods-enabled/vhost_alias.load
fi

sudo rm -rf /etc/apache2/sites-enabled

# Add Vagrant to www-data group
sudo usermod -a -G www-data  vagrant

# Install composer
curl -sS https://getcomposer.org/installer | php
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

# Install PHP Unit
sudo pear config-set auto_discover 1
sudo pear upgrade PEAR
sudo pear install pear.phpunit.de/PHPUnit

mkdir /vagrant/www

# Install WP-CLI
git clone git://github.com/wp-cli/wp-cli.git /vagrant/www/wp-cli
cd /vagrant/www/wp-cli
composer install
sudo ln -sf /vagrant/www/wp-cli/bin/wp /usr/local/bin/wp

# Install PhpMyAdmin
cd /vagrant/www
wget -q https://files.phpmyadmin.net/phpMyAdmin/4.4.12/phpMyAdmin-4.4.12-all-languages.zip
unzip phpMyAdmin-4.4.12-all-languages.zip
rm phpMyAdmin-4.4.12-all-languages.zip
ln -s phpMyAdmin-4.4.12-all-languages pma

# Install Ruby
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash
source /usr/local/rvm/scripts/rvm
rvm requirements
rvm install 2.2.0
sudo chown -R vagrant /usr/local/rvm

# Install NodeJS
sudo apt-get install -y nodejs
sudo apt-get install -y npm
sudo ln -s /usr/bin/nodejs /usr/bin/node

# Restart servers
sudo service apache2 restart
sudo service mysql restart

# Automatically change to the source code directory on SSH connection
echo "cd /vagrant" >> /home/vagrant/.bashrc
