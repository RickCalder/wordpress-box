# WordPress Development Environment

Hello WordPress developer, we have a treat for you. This vagrant box is an excellent portable environment for WordPress development.

It includes only the software you need to do your best WordPress development. And because it is provisioned with a bash script, there is no complicated dev-ops tools to learn.

Please contribute back anything that makes this project better AND easier to use.

## Installation

This project makes use of net-ssh. Run `bundle install` before proceeding.

## Configuring Mac OS X

Before directly downloading this project make sure to setup your dev machine with this script http://gitlab.wiseworkbench.com/developers/development-computer-setup. This will setup your machine with all the required libraries and will forward all requests to `*.dev` to the vagrant server running this this environment

#### Network access

Testing your local copy of a site on a test device (e.g. iPhone) can be useful. The above setup only resolves domains to on your local network interface. Other devices on the network won't be able to access the site without adding your IP address to their /etc/hosts, which is inconvenient or impossible (e.g. on an iPhone).

1. Assign a static IP address to your MAC address
2. Find your local IP address with `ifconfig`
3. Change `address=/wp/127.0.0.1` to `address=/wp/$YOUR_LOCAL_IP_ADDRESS` in `$(brew --prefix)/etc/dnsmasq.conf`
4. Add `listen-address=0.0.0.0` to `$(brew --prefix)/etc/dnsmasq.conf`
5. Restart your computer
6. Check if it works: `dig pma.wp @$YOUR_LOCAL_IP_ADDRESS`. This should return an answer section with your local IP address.

You can then add your local IP address as a DNS server with the highest precedence. On an iPad this can be on in Settings -> Wi-Fi -> info icon on the connected Wi-Fi network -> DNS (mine looks like `10.0.2.19, 10.0.2.1`, your local IP must come first).

## Using this environment

Run `vagrant up` to spin up the virtual machine. This will provision the box the first time this command is run. Run `vagrant ssh` to SSH into the environment. Finally, run `vagrant halt` to shut it down.

#### Projects

All projects should be symlinked from your dev machine's system into the /server/www directory in this project. Download each project from your `Jobs` directory with the git command `git clone <ssh-url-here> ./<project-docket>`. This will download the project to the directory `<project-docket>` and also server the project at `<project-docket>.dev` in your browser.

## Troubleshooting

### Check if dnsmasq is resolving correctly

To determine if dnsmasq is correctly resolving, you can tell `dig` to use `127.0.0.1` as the DNS server, thus pointing to dnsmasq:

`dig pma.wp @127.0.0.1`

You should see a result like the following.

```
;; ANSWER SECTION:
pma.wp.			0	IN	A	127.0.0.1
```

### 403 Forbidden issues

Try making the following modifications to `/private/etc/apache2/extra/httpd-vhosts.conf`:

After

```
<VirtualHost *:80>
  ProxyRequests Off
  ProxyPreserveHost On
```

Add

```
  <Directory />
    AllowOverride All
    Options Indexes MultiViews
    Options +FollowSymLinks
    Require all granted
  </Directory>
```

Remember to reboot the apache on the host, `sudo apachectl restart`

### Ensure guest apache service is running

```
vagrant global-status
vagrant ssh id
```

In guest:

```
sudo service apache2 status
sudo service apache2 start
```

### Determining whether the issue lies with Vagrant Guest or Host Proxy

If you're getting a blank response or white page, it can be difficult to
determine if the problem lies with Apache running on the guest vagrant or with
the proxy setup on the host Apache instance (OS X).

This is where `telnet` can come in handy!!

Say we have some site `wise.wp`.

### Check to see if the site is working inside the guest

```
vagrant@vagrant:~ $ vagrant ssh
vagrant@vagrant:~ $ telnet 127.0.0.1 80
GET / HTTP/1.1
Host: wise.wp


vagrant@vagrant:~ $ # Below telnet is the standard input typed into your
vagrant@vagrant:~ $ # keyboard, including the double newline. This is an HTTP
vagrant@vagrant:~ $ # request specifying the Host manually.
```

### Check to see if the host can access the site without the proxy

```
emily@osx-host:~ $ telnet 127.0.0.1 4678
GET / HTTP/1.1
Host: wise.wp


emily@osx-host:~ $
```

Port `4678` refers to the port being forwarded through Vagrant and then proxied
on the host. Refer to your host Apache configuration and `Vagrantfile`.

### Cisco AnyConnect VPN Client

This VPN client leaves a `vpnagentd` process running even if you're not
connected to a VPN. There was an issue with telnet on the host getting the
correct response but not curl or the browser (empty reply).

#### Check if process is running

```
$ ps aux | grep vpnagentd
```

#### Make it go away

```
$ sudo launchctl unload /Library/LaunchDaemons/com.cisco.anyconnect.vpnagentd.plist
```

`launchctl remove` is also fun. `killall`, `kill -9` or similar _will not work_.

### Other

Nick originally had this in the `/etc/resolver/wp` file but after running into trouble it was removed. A future somebody may find it useful:

`sudo bash -c 'echo "port 35353" >> /etc/resolver/wp'`

### Upgrading to El Capitan Issues/Resolutions:
This may or may not apply to you, but if you find that your *.wp proxy setup has stopped working you will need to uncomment the `LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so` line from httpd.conf in /etc/apache2 as well as `Include /private/etc/apache2/extra/httpd-vhosts.conf`
