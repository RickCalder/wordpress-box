# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # Use Ubuntu 14.04
  config.vm.box = "ubuntu/trusty64"

  # Forward over the SSH auth agent
  config.ssh.forward_agent = true

  # Forward port 80 on guest machine to port 4678 on host machine
  config.vm.network "forwarded_port", guest: 80, host: 4678

  # Sync the server folder on the host machine with the vagrant folder
  config.vm.synced_folder "server", "/vagrant", owner: "vagrant", group: "www-data"
  config.vm.synced_folder "jobs", "/vagrant/www", owner: "vagrant", group: "www-data"
  config.vm.synced_folder "libs/sites-enabled", "/etc/apache2/sites-enabled", owner: "vagrant", group: "www-data"

  # Sync over your ssh details
  config.vm.synced_folder "~/.ssh/","/vagrant/.ssh"

  config.vm.provider "virtualbox" do |vb|
    # Name of the VM
    vb.name = "Sandbox"

    # Amount of memory allocated to the VM
    vb.memory = "1024"
  end

  # Provision the virtual machine with a shell script
  config.vm.provision "shell", path: "provision.sh"

  # Run commands after booting
  config.trigger.after :up do
    run_remote "service apache2 start"
    run_remote "eval `ssh-agent -s`"
    run_remote "ssh-add -k /vagrant/.ssh/id_rsa"
  end
end
